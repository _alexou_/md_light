async function getAuhorChapters(authorID) {
  let mangaList = await fetch(`/author/${authorID}/feed`,);
  let manga = await mangaList.text();
  console.log(await mangaList)
  changeAuthorManga(manga)

}
// loads the mangas created by the author
function changeAuthorManga(content) {
  console.log("content", content)
  let manga_div = document.getElementById("works")
  // console.log(manga_div.innerHTML);
  manga_div.innerHTML = content
}
//gets the titles from the author
async function get_feed(author) {
  let mangaList = await fetch(`/author/feed?${author}`,)
  let manga = await mangaList.text();
  changeAuthorManga(manga)
}

// changes the page link to seatch for a manga
function search() {
  let input = document.getElementById("search_box").value;

  if (input != "") {
    input = `/search/${input}`
    console.log(input)
    window.location.href = input

  } else {
    alert("empty search query")
  }
}

// function to go fullscreen
function goFullscreen() {
  var elem = document.documentElement;

  /* View in fullscreen */
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.webkitRequestFullscreen) { /* Safari */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE11 */
    elem.msRequestFullscreen();
  }

  // exit fullscreen
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.webkitExitFullscreen) { /* Safari */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE11 */
    document.msExitFullscreen();
  }
}
/// fetches chapters for a manga
async function fetch_chapter(mangaID, offset) {
  let chapterPlacement = document.getElementById("chapter_list");
  try {
    document.getElementById("chapter").innerHTML = `<div class="loading"></div>`
    // chapterPlacement.innerHTML = ""
  } catch { }
  console.log(`url:   /chapters/${mangaID}?offset=${offset}`);
  let html = await fetch(`/chapters/${mangaID}?offset=${offset}`);
  let resp = await html.text()
  chapterPlacement.innerHTML = resp
}


async function fetch_search(query, offset) {
  let chapterPlacement = document.getElementById("search-results");
  try {
    document.getElementById("chapter").innerHTML = `<div class="loading"></div>`
    // chapterPlacement.innerHTML = ""
  } catch { }
  console.log(`url:   /search/results?query=${query}&offset=${offset}`);
  let html = await fetch(`/search/results?query=${query}&offset=${offset}`);
  let resp = await html.text()
  chapterPlacement.innerHTML = resp
}


/// creates a formatted url for downloading chapters
function constructURL(id) {
  const form = document.getElementById('chapterForm');
  const checkboxes = form.querySelectorAll('input[name="chapters"]:checked');

  if (checkboxes.length === 0) {
    alert('Please select at least one chapter.');
    return;
  }

  let chapters = [];
  checkboxes.forEach(checkbox => {
    chapters.push(checkbox.chapter_id);
    // chapters.push(checkbox.value);
  });

  const url = `/ddl?chapters=${chapters.join(',')}&manga_id=${id}&language=en`;
  // alert('Generated URL: ' + url);
  location.href = url
  // Here you can use this URL, for example, you can redirect the user to this URL
  // window.location.href = url;
}


async function constructAdvancedSearchURL() {
  const form = document.getElementById('SearchForm');
  const included = form.querySelectorAll('input[name="included_tags"]:checked');
  const excluded = form.querySelectorAll('input[name="excluded_tags"]:checked');
  // console.log(form.innerHTML)

  let included_tags = [];
  included.forEach(checkbox => {
    included_tags.push("include_tag[]="+checkbox.value);
    console.log(checkbox.value)
  });

  let excluded_tags = [];
  excluded.forEach(checkbox => {
    excluded_tags.push("exclude_tag[]="+checkbox.value);

  });
  let jspn = {
    'included_tags': included_tags,
    'excludedTags': excluded_tags
  }
  // let resp = await fetch('/advanced', {
  //   method:'GET',
  //   query: JSON.stringify({
  //     included_tags: included_tags,
  //     excludedTags: excluded_tags
  //   }),

  // })
  // console.log(await resp.url)
  // let t = await resp.text()


  const url = `/advanced?${included_tags.join('&')}&${excluded_tags.join('&')}`;
  let resp = await fetch(url)
  let t = await resp.text()
  form.innerHTML = t
  // location.href = url
  // Here you can use this URL, for example, you can redirect the user to this URL
  // window.location.href = url;
}


async function showDdlOverlay(id, lang) {
  let tt = document.getElementById("over");
  let window = document.getElementById("download-overlay");
  // window.style.display = "block"
  console.log("ds")
  let resp = await fetch(`/download/${id}?offset=0&language=en`)
  let html = await resp.text();
  window.innerHTML = html;
  tt.style.display = "block"
}

function closeForm() {
  document.getElementById("over").style.display = "none"
}


async function saveManga(id) {
  fetch(`/save/${id}`, {
    method: "post",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
    .then((response) => {
      document.getElementById("saved").style.display = "block"
      document.getElementById("unsaved").style.display = "none"
    });
}
async function unsaveManga(id) {
  fetch(`/unsave/${id}`, {
    method: "post",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
    .then((response) => {
      document.getElementById("saved").style.display = "none"
      document.getElementById("unsaved").style.display = "block"
    });
}
async function save_dict(id, name, cover_url) {
  // id="123"

  let params = "?id=" + id + "&name=" + name + "&cover_url=" + cover_url
  fetch("/save" + params, {
    method: "post",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },

  })
    .then((response) => {
      document.getElementById("saved").style.display = "block"
      document.getElementById("unsaved").style.display = "none"
    });
}

async function unsave_dict(id) {
  fetch(`/unsave/${id}`, {
    method: "post",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
    .then((response) => {
      document.getElementById("saved").style.display = "none"
      document.getElementById("unsaved").style.display = "block"
    });
}