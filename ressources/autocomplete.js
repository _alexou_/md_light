window.onload = function () {
    var x = document.getElementById('search_box');
    x.addEventListener('input', function () {
        
        changeContent(x.value)
    });
}


async function changeContent(results) {
    var field = document.getElementById("autocomplete-container");
    // hides the autocomplete elements if the search is empty
    if (results == "" ) {
        field.innerHTML = "";
    } else if(results.length >3){
        let content = await fetch(`/autocomplete?query=${results}`);
        let resp = await content.text();
        field.innerHTML = resp;
    }
}