use crate::{api_error::ApiError, md_struct::Tag, online_md, req_params::ReqParam};
use lazy_static::lazy_static;
use std::{collections::HashMap, sync::RwLock};

type TagId = String;

lazy_static! {
    // static ref TAGS: RwLock<HashMap<String, TagId>> = RwLock::new(HashMap::new());
    static ref TAGS: RwLock<Vec<Tag>> = RwLock::new(Vec::new());
}

pub async fn fetch_all_tags() -> Result<(), ApiError> {
    let url = "https://api.mangadex.org/manga/tag";
    let json = online_md::request_with_params::<ReqParam>(url.to_string(), vec![]).await?;
    let arr = json["data"].as_array().unwrap();
    for tag in arr {
        let tag_name = tag["attributes"]["name"]["en"].as_str().unwrap();
        let tag_id = tag["id"].as_str().unwrap();
        let tag = Tag{id:tag_id.to_string(), name:tag_name.to_string()};
        TAGS.write()
            .unwrap()
            .push(tag);
    }
    println!("tags finished fetching");
    Ok(())
}
pub fn get_all_tags()-> Vec<Tag>{
    let max = TAGS.read().unwrap().len();
 TAGS.read().unwrap().get(0..max-1).unwrap().to_vec()
}