use std::thread;
use std::time::Duration;

// contains all the endpoints of the website
use crate::api_error;
use crate::cache_pages;
use crate::cli_options::*;
// use crate::downloader;
use crate::online_md;
use crate::query_struct;
use crate::query_struct::*;
use crate::req_params::*;
use crate::save_manga;
use crate::tera_templates;
use crate::utills;
use actix_web::http;
use actix_web::{
    get,
    http::StatusCode,
    post,
    web::{self, Redirect},
    HttpRequest, HttpResponse, Responder, Result,
};
use openssl::pkey::Params;
use serde::Deserialize;
use serde::Serialize;
use serde_qs::Config;
use tera_templates::*;

/// handles any pages that aren't handled by the
pub async fn not_found() -> impl Responder {
    // actix_web::web::Redirect::to("../").see_other()
    println!("page not found: ");
    tera_templates::render_error_page(api_error::ApiError::ApiPageNotFound404, "page_not_found")
    // HttpResponse::NotFound().body("Page not found.")
}
#[get("/error")]
async fn send_error() -> HttpResponse {
    // actix_web::web::Redirect::to("../").see_other()
    let tt = tera_templates::render_error_page(api_error::ApiError::ApiPageNotFound404, "sdf");
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(tt)
    // HttpResponse::NotFound().body("Page not found.")
}

/// the homepage
#[get("/")]
async fn index(path: HttpRequest) -> HttpResponse {
    let is_localhost = utills::check_localhost(&path);
    let feed = online_md::get_md_homepage_feed(CONFIG.datasaver).await;

    // handles the errors by sending the error page
    // let html = match feed {
    //     Ok(e) => manga_templates::render_homepage(e, is_localhost),
    //     Err(v) => manga_templates::render_error_page(v, "/"),
    // };

    let html = match feed {
        Ok(e) => tera_templates::render_homepage(e, is_localhost),
        Err(v) => tera_templates::render_error_page(v, "/"),
    };

    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

#[get("/manga/{id}")]
async fn get_manga_info(manga_id: web::Path<String>, path: HttpRequest) -> HttpResponse {
    let requested_page = path.path();
    let is_localhost = utills::check_localhost(&path);

    let manga_info = online_md::get_manga_info(manga_id.to_string(), CONFIG.datasaver);

    // handles the errors by sending the error page
    let html = match manga_info.await {
        // Ok(e) => manga_templates::render_manga_info_page(e, is_localhost),
        Ok(e) => tera_templates::render_manga_info(e, is_localhost),
        Err(v) => tera_templates::render_error_page(v, requested_page),
    };
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

#[get("/chapters/{id}")]
async fn get_chapter_list(
    manga_id: web::Path<String>,
    path: HttpRequest,
    infos: web::Query<ChapterQuery>,
) -> HttpResponse {
    let requested_page = path.path();
    let is_localhost = utills::check_localhost(&path);

    let chapters = online_md::get_manga_chapters(
        &manga_id.to_string(),
        &infos.language.clone(),
        infos.offset,
        OrderingValues::Ascending,
    )
    .await;

    // throws the error page if there is an error
    let chapters = match chapters {
        Ok(e) => e,
        Err(v) => {
            let html = tera_templates::render_error_page(v, requested_page);
            return HttpResponse::build(StatusCode::OK)
                .content_type("text/html; charset=utf-8")
                .body(html);
        }
    };
    // let chapters = chapters.unwrap();
    let html = tera_templates::render_manga_chapter_list(
        chapters,
        infos.offset,
        manga_id.to_string(),
        is_localhost,
        query_struct::ChapterRenderMode::Normal,
    );

    // handles the errors by sending the error page
    let html = match html {
        // Ok(e) => manga_templates::render_manga_info_page(e, is_localhost),
        Ok(e) => e,
        Err(v) => tera_templates::render_error_page(v, requested_page),
    };

    // let html = "234";
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

// returns the chapter's pages
#[get("/manga/{manga}/{chapter}/{chapter_number}/{language}")]
async fn get_chapter(
    chapter: web::Path<(String, String, i64, String)>,
    path: HttpRequest,
) -> HttpResponse {
    let is_localhost = utills::check_localhost(&path);

    let manga_id = chapter.0.to_string();
    let chapter_id = chapter.1.to_string();
    let chapter_number = chapter.2;
    let language = chapter.3.to_string();
    println!("{}", chapter_number);

    let chapter_info = online_md::get_chapter_pages(chapter_id.clone()).await;
    let infos = online_md::get_prev_and_next_chapters(
        chapter_id,
        chapter_number,
        manga_id.clone(),
        language,
    )
    .await
    .expect("can't get next and previous chapters");

    let html = match chapter_info {
        // Ok(e) => manga_templates::render_chapter(e, is_localhost, manga_id),
        Ok(e) => render_chapter_view(e, is_localhost, infos, manga_id).await,
        Err(v) => tera_templates::render_error_page(v, path.path()),
    };
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

#[get("/search/results")]
async fn get_search_results(
    path: HttpRequest,
    params: serde_qs::actix::QsQuery<SearchQuery>,
) -> HttpResponse {
    let is_localhost = utills::check_localhost(&path);
    let search_query = &params.query;

    let query = search_query.clone().unwrap_or("".to_owned());
    println!("search for: {}", query);
    let mut req_params = vec![SEARCH_TITLE(query.clone())];

    if let Some(tag) = &params.include_tag {
        req_params.extend(filter_multiple_tags(true, tag.clone()));
    }
    if let Some(tag) = &params.exclude_tag {
        req_params.extend(filter_multiple_tags(false, tag.clone()));
    }

    let manga_results = online_md::search_manga(req_params, CONFIG.datasaver, params.offset).await;
    // let author_results = online_md::search_author(query.to_string()).await;

    // concats the manga result and the author result list onto a single vector
    // let search_result = manga_results.and_then(|a| author_results.map(|b| (a, b)));

    let html = match manga_results {
        // Ok(e) => manga_templates::render_complete_search(e, is_localhost, query.to_string()),
        Ok(e) => tera_templates::render_search_templete(
            e,
            query,
            params.offset.unwrap_or(0),
            is_localhost,
        ),
        Err(v) => tera_templates::render_error_page(v, path.path()),
    };

    // let html = match manga_results {
    //     Ok(e) => tera_templates::render_complete_search(e, query.to_string()),
    //     Err(v) => manga_templates::render_error_page(v, path.path()),
    // };
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}


// searches for a manga
#[get("/search")]
async fn search(path: HttpRequest, params: serde_qs::actix::QsQuery<SearchQuery>) -> HttpResponse {
    // let is_localhost = utills::check_localhost(&path);
    let search_query = &params.query;
    println!("{:#?}", params);
    let query = search_query.clone().unwrap_or("".to_owned());
    // let manga_results =
    //     online_md::search_manga(search_query.to_owned(), None, CONFIG.datasaver, params.offset).await;
    let author_results = online_md::search_author(query.to_string()).await;

    // // concats the manga result and the author result list onto a single vector

    let html = match author_results {
        Ok(e) => {
            tera_templates::render_author_info(e, query.to_string(), params.offset.unwrap_or(0))
        }
        Err(v) => tera_templates::render_error_page(v, path.path()),

    };

    // let html = tera_templates::render_complete_search(params.query.clone().unwrap());
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

/// searches dor manga containing a tag
#[get("/tags")]
async fn search_for_tag(path: HttpRequest, params: web::Query<SearchQuery>) -> HttpResponse {
    println!("search for: ");

    let is_localhost = utills::check_localhost(&path);
    let mut req_params = vec![];
    if let Some(tag) = &params.include_tag {
        req_params.extend(filter_multiple_tags(true, tag.clone()));
    }
    // let tags = &params.tag.clone().unwrap_or("".to_string()).to_string();

    let manga_results = online_md::search_manga(req_params, CONFIG.datasaver, params.offset).await;
    // online_md::search_manga(None, Some([("includedTags[]=", search_query.to_owned())]), CONFIG.datasaver).await;

    // let html = match manga_results {
    //     // Ok(e) => manga_templates::render_complete_search(e, is_localhost, query.to_string()),
    //     Ok(e) => tera_templates::render_complete_search((e,vec![]), search_query.to_string(), is_localhost),
    //     Err(v) => tera_templates::render_error_page(v, path.path()),
    // };
    let html = match manga_results {
        Ok(e) => tera_templates::render_profile_info(e, "tag search".to_string(), is_localhost),
        Err(v) => tera_templates::render_error_page(v, path.path()),
    };
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

// searches for a manga
#[get("/autocomplete")]
async fn autocomplete(path: HttpRequest, params: web::Query<SearchQuery>) -> HttpResponse {
    let is_localhost = utills::check_localhost(&path);
    let search_query = &params.query;
    let query = search_query.clone().unwrap_or("".to_string());
    println!("search for: {}", query);

    let manga_results =
        online_md::search_manga(vec![SEARCH_TITLE(query.clone())], true, None).await;

    let html = match manga_results {
        // Ok(e) => manga_templates::render_complete_search(e, is_localhost, query.to_string()),
        Ok(e) => tera_templates::render_autocomplete(e, query.to_string(), is_localhost),
        Err(v) => tera_templates::render_error_page(v, path.path()),
    };
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

#[get("/advanced")]
async fn advanded_search() -> HttpResponse {
    let html = tera_templates::render_advanced_search();
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

// searches for a manga
#[get("/author/{author_id}/{author_name}")]
async fn get_author(params: web::Path<(String, String)>, path: HttpRequest) -> HttpResponse {
    // let author_data = online_md::get_author_infos(author_id.to_string()).await;
    // // handles the errors by sending the error page
    // let html = match author_data {
    //     Ok(e) => manga_templates::render_author_page(e),
    //     Err(v) => manga_templates::render_error_page(v, path.path()),
    // };

    let is_localhost = utills::check_localhost(&path);

    let author_id = params.0.to_string();
    let author_name = params.1.to_string();

    let manga_list = online_md::search_manga(
        vec![FILTER_AUTHOR(author_id)],
        // Some([("authorOrArtist", author_id.to_string())]),
        CONFIG.datasaver,
        None,
    )
    .await;

    // handles the errors by sending the error page
    let html = match manga_list {
        Ok(e) => tera_templates::render_profile_info(e, author_name, is_localhost),
        Err(v) => tera_templates::render_error_page(v, path.path()),
    };

    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

// #[get("/author/{author_id}/feed")]
// async fn get_author_feed(author_id: web::Path<String>, path: HttpRequest) -> HttpResponse {
//     let is_localhost = utills::check_localhost(&path);
// println!("author");

//     let manga_list = online_md::search_manga(
//         None,
//         Some([("authorOrArtist", author_id.to_string())]),
//         CONFIG.datasaver,
//     )
//     .await;

//     // handles the errors by sending the error page
//     let html = match manga_list {
//         Ok(e) => manga_templates::render_author_manga(e, is_localhost),
//         Err(v) => manga_templates::render_error_page(v, path.path()),
//     };
//     HttpResponse::build(StatusCode::OK)
//         .content_type("text/html; charset=utf-8")
//         .body(html)
// }

#[get("/server")]
async fn get_server_options() -> HttpResponse {
    let html = tera_templates::get_server_options();

    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

// pings the mangadex server to test connection
#[get("/server/ping")]
async fn ping_md() -> impl Responder {
    match online_md::test_connection().await {
        Ok(status) => {
            format!(
                r"
        reachable: {}
        server up: {}
        ",
                status.reachable, status.up
            )
        }
        Err(v) => format!("internal server error: {}", v),
    }
}

#[get("/server/loop")]
async fn loop_for() -> impl Responder {
    for _ in 0..30 {
        thread::sleep(Duration::from_millis(1000))
    }

    ""
}

// kills the server
#[get("/server/kill")]
async fn kill_server() -> impl Responder {
    // let restrict = CONFIG.secure;
    // // allows killing the server only if the restrict option is on and the client is the host or if the  restrict option is false
    // if (utills::check_localhost(&path)) || !restrict {
    println!("The server was killed with exit code 1");
    std::process::exit(1);
    // } else {
    //     // prints a message
    //     println!(
    //         "Unauthorized access to /server/kill: {}",
    //         path.connection_info()
    //             .peer_addr()
    //             .expect("unable to get client IP")
    //             .on_red()
    //     );
    //     format!(
    //         "You do not have the permission to kill the server\nIP address: {}",
    //         path.connection_info()
    //             .peer_addr()
    //             .expect("unabel to get client IP")
    //     )
    // }
    // ""
    HttpResponse::ExpectationFailed()
}

#[get("/group/{id}/{name}")]
async fn get_group(data: web::Path<(String, String)>, path: HttpRequest) -> HttpResponse {
    let is_localhost = utills::check_localhost(&path);

    let group_id = data.0.to_string();
    let group_name = data.1.to_string();

    let manga_list = online_md::search_manga(
        vec![FILTER_GROUP(group_id)],
        // Some([("group", group_id.to_string())]),
        CONFIG.datasaver,
        None,
    )
    .await;

    // handles the errors by sending the error page
    let html = match manga_list {
        Ok(e) => tera_templates::render_profile_info(e, group_name, is_localhost),
        Err(v) => tera_templates::render_error_page(v, path.path()),
    };
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

#[get("/download/{id}")]
async fn get_download_form(
    manga_id: web::Path<String>,
    path: HttpRequest,
    infos: web::Query<ChapterQuery>,
) -> HttpResponse {
    let requested_page = path.path();
    let is_localhost = utills::check_localhost(&path);

    let chapters = online_md::get_manga_chapters(
        &manga_id.to_string(),
        &infos.language.clone(),
        infos.offset,
        OrderingValues::Ascending,
    )
    .await;

    let chapters = match chapters {
        // Ok(e) => manga_templates::render_manga_info_page(e, is_localhost),
        Ok(e) => e,
        Err(v) => {
            let html = tera_templates::render_error_page(v, requested_page);
            return HttpResponse::build(StatusCode::OK)
                .content_type("text/html; charset=utf-8")
                .body(html);
        }
    };

    let html = tera_templates::render_manga_chapter_list(
        chapters,
        infos.offset,
        manga_id.to_string(),
        is_localhost,
        query_struct::ChapterRenderMode::Download,
    );

    // handles the errors by sending the error page
    let html = match html {
        // Ok(e) => manga_templates::render_manga_info_page(e, is_localhost),
        Ok(e) => e,
        Err(v) => tera_templates::render_error_page(v, requested_page),
    };

    // let html = "234";
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

#[derive(Deserialize)]
struct Info {
    username: Vec<String>,
}
#[get("/test")]
async fn test_ddl() -> impl Responder {
    let html = tera_templates::render_advanced_search();
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}
/// saves tha manga to a special list
#[post("/save/{manga_id}")]
async fn save_title(id: web::Path<String>) -> impl Responder {
    save_manga::save_manga(id.to_string());
    HttpResponse::Ok()
}

/// removes tha manga from a special list
#[post("/unsave/{manga_id}")]
async fn unsave_title(id: web::Path<String>) -> impl Responder {
    // save_manga::unsave_manga(id.to_string());
    save_manga::unsave_to_dict(id.to_string());
    HttpResponse::Ok()
}

#[get("/saved")]
async fn get_saved_titles(path: HttpRequest) -> HttpResponse {
    let is_localhost = utills::check_localhost(&path);

    let html = tera_templates::render_saved_titles(is_localhost);

    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}

#[post("/save")]
async fn save_dict_title(data: web::Query<save_manga::BookmarkedTitles>) -> impl Responder {
    println!("{:?}", data.0);
    save_manga::save_to_dict(data.0);
    HttpResponse::Ok()
}

/// removes tha manga from a special list
#[post("/unsave/{manga_id}")]
async fn unsave_dict_title(id: web::Path<String>) -> impl Responder {
    save_manga::unsave_manga(id.to_string());
    HttpResponse::Ok()
}

#[post("/download")]
async fn download_manga(web::Form(form): web::Form<DownloadQuery>) -> Redirect {
    println!("{:#?}", form);
    actix_web::web::Redirect::to("../").see_other()
}

/// proxies the images url when using the --lan argument for the server
pub async fn image_proxy(image_url: web::Path<String>) -> Result<HttpResponse> {
    let image_url = image_url.into_inner();
    // checks if the image is stored in server cache
    match cache_pages::get_page(&image_url) {
        Some(url) => Ok(HttpResponse::Ok().body(url)),
        None => {
            let response = online_md::CLIENT.get(&image_url).send().await;

            match response {
                Ok(resp) => {
                    let bytes = resp.bytes().await;
                    match bytes {
                        Ok(image_byte) => {
                            // adds the image to cache
                            cache_pages::insert_page(image_url, &image_byte);

                            Ok(HttpResponse::Ok().body(image_byte))
                        }
                        // returns an empty image in case of an error
                        Err(_) => Ok(HttpResponse::NotFound().finish()),
                    }
                }
                Err(_) => {
                    // Return an error response or a placeholder image
                    Ok(HttpResponse::NotFound().finish())
                }
            }
        }
    }
}
