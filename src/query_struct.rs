use serde::de;
use serde::de::IntoDeserializer;
use serde_derive::{Serialize,Deserialize};
//use serde::{Serialize, Deserialize};

use std::fmt;

use crate::language::Language;

/// used when loading the chapter for a manga
#[derive(serde::Deserialize)]
pub struct ChapterQuery {
    pub offset: i32,
    pub language: Option<String>,
}
/// used when searching for a manga or author
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct SearchQuery {
    pub query: Option<String>,
    pub offset: Option<i32>,
    pub ordering: Option<String>,
    pub include_tag: Option<Vec<String>>,
    pub exclude_tag: Option<Vec<String>>,
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ta {
    #[serde(deserialize_with = "deserialize_stringified_list")]
    pub tag: Vec<String>,
}
///
#[derive(Serialize, Deserialize, Debug)]
pub struct DownloadQuery {
    pub id: String,
    pub lang: Language,
    /// download imaged in low resolution. False and none will download images in high-res
    pub low_res: Option<bool>,
    pub offset: Option<i32>,
}

#[derive(Serialize, Deserialize, Debug)]
/// used to tell what chapters to download
pub enum DownloadMode {
    /// downloads all of the chapters. this will re-download existing chapters
    All,
    /// downlads the chapters in a certain range eg: chapters 5 to 10. Uses offset and end
    Range,
    /// downloads the chapters that aren't already downloaded. will also download older chapters that aren't downloaded
    CheckExisting,
    /// only download newer chapters
    Update,
}
#[derive(Serialize, Deserialize, Debug)]
/// used to render the list of chapters for download or for reading
pub enum ChapterRenderMode {
    /// reading mode: clicking will open chapter
    Normal,
    /// download mode: clicking will select the chapter for downloading
    Download,
}
#[derive(Serialize, Deserialize, Debug)]
pub struct DdlTest {
    #[serde(deserialize_with = "deserialize_stringified_list")]
    pub chapters: Vec<String>,
    pub manga_id: String,
    pub language: String,
}

pub fn deserialize_stringified_list<'de, D, I>(
    deserializer: D,
) -> std::result::Result<Vec<I>, D::Error>
where
    D: de::Deserializer<'de>,
    I: de::DeserializeOwned,
{
    struct StringVecVisitor<I>(std::marker::PhantomData<I>);

    impl<'de, I> de::Visitor<'de> for StringVecVisitor<I>
    where
        I: de::DeserializeOwned,
    {
        type Value = Vec<I>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a string containing a list")
        }

        fn visit_str<E>(self, v: &str) -> std::result::Result<Self::Value, E>
        where
            E: de::Error,
        {
            let mut ids = Vec::new();
            for id in v.split(',') {
                let id = I::deserialize(id.into_deserializer())?;

                ids.push(id);
            }
            Ok(ids)
        }
    }

    deserializer.deserialize_any(StringVecVisitor(std::marker::PhantomData::<I>))
}
