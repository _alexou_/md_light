use crate::api_error::ApiError;
use crate::deserializer::MangaDeserializer;
use crate::md_struct::*;
use crate::query_struct::SearchQuery;
use crate::req_params;
use crate::req_params::*;
use crate::utills::*;
use lazy_static::lazy_static;
use openssl::pkey::Params;
use reqwest::Client;
use serde::Serialize;
use serde_json::{from_str, Value};
use std::borrow::Borrow;
use std::time::Duration;
use std::vec;

const BASE_URL: &str = "https://api.mangadex.org";
const MANGA_BASE_URL: &str = "https://api.mangadex.org/manga";
// const LIMIT: [(&str, i32); 1] = [("limit", 100)];
// const CHAPTER_ORDERING: [(&str, &str); 1] = [("order[chapter]", "asc")];
// const INCLUDE_TL_GROUP: [(&str, &str); 1] = [("includes[]", "scanlation_group")];

lazy_static! {
    pub static ref CLIENT: Client = Client::builder()
        .user_agent("Md_light")
        .build()
        .expect("unable to build Client");
}

/// sends a get request to the /ping endpoint of the api
pub async fn test_connection() -> Result<ServerStatus, ApiError> {
    // making a get request to the server
    let resp = request_with_params::<ReqParam>(format!("{BASE_URL}/ping"), vec![]).await;

    let reachable = resp.is_ok();

    // true only if the server response is "pong", otherwise the server is down
    let up = if reachable {
        let resp_content = &resp?;

        resp_content.as_str() == Some("pong")
    } else {
        false
    };

    let status = ServerStatus { up, reachable };
    Ok(status)
}

/// new function to make api requests
pub async fn request_with_params<T: Serialize>(
    url: String,
    params: Vec<T>,
    // optional_param: Option<req_params::ReqParam>,
) -> Result<Value, ApiError> {
    for _ in 0..2 {
        let mut req = CLIENT
            .get(&url)
            .query(&INCLUDE_PARAM(IncludedValues::Cover));
        for p in &params {
            req = req.query(&p);
        }

        let response = req.send().await?;
        let response = response.text().await?;
        // converting the text response into a json value
        let json_res_result = from_str(&response);
        match json_res_result {
            Ok(json) => return Ok(json),
            Err(_) => {
                std::thread::sleep(Duration::from_millis(200));
            }
        };
    }
    Err(ApiError::ApiResponseError)
}

/// gets the informations for the homepage
pub async fn get_md_homepage_feed(is_low_res: bool) -> Result<MdHomepageFeed, ApiError> {
    let popular_future = std::thread::spawn(move || get_popular_manga(is_low_res));
    let updated_manga_future = std::thread::spawn(move || get_updated_manga(is_low_res));
    // builds the struct for the popular titles+ new chapters
    let homepage_feed = MdHomepageFeed {
        currently_popular: popular_future.join()?.await?,
        new_chapter_releases: updated_manga_future.join()?.await?,
    };

    Ok(homepage_feed)
}

/// gets the list of the last updated titles
pub async fn get_updated_manga(is_low_res: bool) -> Result<SearchResults, ApiError> {
    let params = vec![
        INCLUDE_PARAM(IncludedValues::Cover),
        FILTER_RATING(ContentRating::Safe),
        FILTER_RATING(ContentRating::Erotica),
        FILTER_RATING(ContentRating::Suggestive),
        LIMIT(20),
    ];

    let json_resp = request_with_params(MANGA_BASE_URL.to_string(), params).await?;

    json_resp.to_search_results(is_low_res)
}

// gets the most popular mangas from the last month for the homepage feed
pub async fn get_popular_manga(is_low_res: bool) -> Result<SearchResults, ApiError> {
    // let mut popular_manga: Vec<PopularManga> = Vec::new();

    let params = vec![
        INCLUDE_PARAM(IncludedValues::Cover),
        FILTER_RATING(ContentRating::Safe),
        FILTER_RATING(ContentRating::Erotica),
        FILTER_RATING(ContentRating::Suggestive),
        ORDERING(OrderBy::FollowedCount, OrderingValues::Descending),
        HAS_AVAILABLE_CHAPS(true),
        // GET_RECENT(),
        LIMIT(10),
    ];
    let url = format!("{MANGA_BASE_URL}?createdAtSince={}", get_offset_time());
    let json_resp = request_with_params(url, params).await?;

    json_resp.to_search_results(is_low_res)
}

/// searches for a manga. Can also use parameters to search for authors or more
pub async fn search_manga(
    // title: Option<String>,
    arg_params: Vec<ReqParam>,
    is_low_res: bool,
    offset: Option<i32>,
) -> Result<SearchResults, ApiError> {
    // if params.is_some() { todo!() }
    let offset = offset.unwrap_or(0);
    // let title_param = if let Some(t) = title {
    //     SEARCH_TITLE(t)
    // } else {
    //     None
    // };
    let mut params = vec![
        INCLUDE_PARAM(IncludedValues::Cover),
        // INCLUDE_PARAM(IncludedValues::Author),
        // INCLUDE_PARAM(IncludedValues::Artist),
        LIMIT(20),
        OFFSET(offset),
        // title_param,
    ];
    params.extend(arg_params);
    let json_resp = request_with_params(MANGA_BASE_URL.to_string(), params).await?;
    // deserializes the json data
    json_resp.to_search_results(is_low_res)
}

/// searches for an author
pub async fn search_author(query: String) -> Result<Vec<AuthorInfo>, ApiError> {
    let url = format!("{BASE_URL}/author");

    // does the request and converts it to json
    let json_resp = request_with_params(url, vec![SEARCH_NAME(query)]).await?;
    // let json_resp: Value = from_str(&resp)?;
    // let json_resp = parse_json(&resp).await?;

    //converting the response data into an array
    let author_list = json_resp["data"]
        .as_array()
        .ok_or("unable to convert author search to array")?;

    let mut author_info_list = vec![];
    // gets the infos for each author in the response
    for author in author_list {
        let id = author["id"].as_str().unwrap_or("no id").to_string();
        let name = author["attributes"]["name"]
            .as_str()
            .unwrap_or("no name")
            .to_string();

        let author_info = AuthorInfo { name, id };
        author_info_list.push(author_info);
    }
    Ok(author_info_list)
}

// gets the cover from the json the request url needs to have includes[]=cover_art for this function to work
pub fn get_manga_cover(
    manga_id: &str,
    // manga_id: &String,
    manga_json: &Value,
    is_low_res: bool,
) -> Result<String, ApiError> {
    //uses the low quality images if the low auality argument is given
    let quality = match is_low_res {
        false => 512,
        true => 256,
    };

    let mut thumbnail = String::new();
    if let Some(manga_cover) = manga_json["relationships"].as_array() {
        for i in manga_cover {
            if i["type"] == "cover_art" {
                let cover_id = i["attributes"]["fileName"].as_str().unwrap();
                let cover_link =
                    format!("https://mangadex.org/covers/{manga_id}/{cover_id}.{quality}.jpg");
                // .replace('"', "");
                thumbnail = cover_link;
                break; //breaks the loop if the cover is found
            }
        }
    }
    Ok(thumbnail)
}

// gets the manga info and chapters
pub async fn get_manga_info(manga_id: String, is_low_res: bool) -> Result<MangaInfo, ApiError> {
    // calls the function to get chapters for a faster page loading

    let url = format!("{MANGA_BASE_URL}/{}", &manga_id);
    // calling the function to make the request to the api
    let params = vec![
        INCLUDE_PARAM(IncludedValues::Artist),
        INCLUDE_PARAM(IncludedValues::Cover),
        INCLUDE_PARAM(IncludedValues::Author),
    ];
    let json_resp = request_with_params(url.clone(), params).await?;
    // parsing the api response into a json
    // let json_resp: Value = from_str(&resp)?;
    // let json_resp = parse_json(&resp).await?;
    // separating the json response to make it easier to access items

    json_resp.to_manga_info(is_low_res)
}

// gets all of the manga's chapters for the manga info page
// returns a vector that contains both errors and chapters
pub async fn get_manga_chapters(
    manga_id: &String,
    language: &Option<String>,
    offset: i32,
    order: req_params::OrderingValues,
) -> Result<MangaChapters, ApiError> {
    let url = format!("{}/{}/feed", MANGA_BASE_URL, manga_id);

    let mut params = vec![
        LIMIT(100),
        OFFSET(offset),
        ORDERING(OrderBy::Chapter, order.clone()),
        exclude_external_url(),
        INCLUDE_PARAM(IncludedValues::ScanlationGroup),
        FILTER_LANG(&"en".to_string()),
    ];
    // todo: allow other chapter languages
    // if let Some(l) = language.as_ref() {
    //     params.push(FILTER_LANG(l));
    // };

    let chapter_json = request_with_params(url, params).await?;
    // let mut json_list: Vec<Value> = vec![]; // a list containing the json data about the chapters
    // println!("{}", chapter_json);

    chapter_json.to_chapters(&order)
}

pub async fn get_chapter_pages(chapter_id: String) -> Result<Vec<String>, ApiError> {
    let url = format!("{}/at-home/server/{}", BASE_URL, chapter_id);
    // let resp = reqwest::get(&url).await?.text().await?;
    let json_resp = request_with_params::<ReqParam>(url, vec![]).await?;

    // let json_resp: Value = from_str(&resp)?;
    // let json_resp = parse_json(&resp).await?;
    let chapter_hash = json_resp["chapter"]["hash"].as_str().unwrap();

    let pages_json = json_resp["chapter"]["data"]
        .as_array()
        .ok_or("there are no pages")?; //transforming the response string into a json object

    let mut page_list: Vec<String> = Vec::new();
    for page in pages_json {
        let mut page_link = page.as_str().unwrap().to_string();
        page_link = format!(
            "https://uploads.mangadex.org/data/{}/{}",
            chapter_hash, page_link
        );
        page_list.push(page_link)
    }

    Ok(page_list)
}

// parses the json response from the api and returns an error if it is invalid
async fn parse_json(response: &str) -> Result<Value, ApiError> {
    let json_resp = from_str(response);
    // checks if the response is of type error
    let json_success: Value = match json_resp {
        Ok(v) => v,
        Err(e) => return Err(ApiError::Json(e)),
    };

    // Ok(json_success)
    let result = json_success["result"].to_owned();
    match result.to_string().as_str() {
        r#""error""# => {
            // println!("{:?}", json_success);
            Err(ApiError::ApiPageNotFound404)
        }
        r#""ok""# => Ok(json_success),
        _ => Err(ApiError::ApiResponseError),
    }
}

pub async fn get_prev_and_next_chapters(
    chapter_id: String,
    chapter_number: i64,
    manga_id: String,
    language: String,
) -> Result<CurrentChapter, ApiError> {
    // let mut offset = get_offset_from_f32(chapter_number);
    let mut offset = chapter_number - 50;
    if offset < 0 {
        offset = 0
    }
    let mut index = None;
    // let mut chapters: Option<MangaChapters> = None;
    // tries another offset if the
    let chapters = Some(
        get_manga_chapters(
            &manga_id,
            &Some(language.clone()),
            offset as i32,
            OrderingValues::Ascending,
        )
        .await?,
    );
    let mut ch_offset = 0;
    // for i in 0..chapters.as_ref().unwrap().chapters.len() {
    for (i, ch) in chapters.as_ref().unwrap().chapters.iter().enumerate() {
        // if chapters.chapters[i]
        let ch = ch.as_ref().unwrap();
        if ch.chapter_id == chapter_id {
            index = Some(i);
            ch_offset = ch.offset;
            break;
        }
    }

    let mut prev = None;
    let mut next = None;
    let total = chapters.as_ref().unwrap().total;
    let ch = chapters.unwrap().chapters;
    println!("first: {:#?}", ch.first());
    println!("last: {:#?}", ch.last());
    let curr_chapter = ch[index.unwrap()].as_ref().unwrap().clone();

    // let curr_chap_number = ch[index]?.chapter_number;
    if !ch.is_empty() {
        for c in ch {
            let c: Chapter = match c {
                Ok(c) => c,
                Err(_) => break,
            };
            if c.chapter_number > curr_chapter.chapter_number {
                next = Some(c.clone());
                break;
            }
            if c.chapter_number < curr_chapter.chapter_number {
                prev = Some(c.clone());
            }
        }
    }

    Ok(CurrentChapter {
        prev,
        next,
        total,
        curr_chapter,
    })
}

/// returns the offset required to get the previous and next chapters
fn get_offset_from_f32(number: f64) -> i32 {
    // if number == "Oneshot" {
    //     return 0;
    // }
    // let number: f32 = from_str(number).unwrap();
    let mut offset = (number - 10.0) as i32;
    if offset < 0 {
        offset = 0;
    }
    println!("offset: {}, num: {}", offset, number);
    offset
}

pub trait Contain {
    fn contains_id(self, id_to_find: String) -> bool;
}

impl Contain for Vec<Result<Chapter, ApiError>> {
    fn contains_id(self, id_to_find: String) -> bool {
        for ch in self {
            if ch.unwrap().chapter_id == id_to_find {
                return true;
            }
        }
        false
    }
}

pub async fn advanded_search(query: SearchQuery) -> Result<SearchResults, ApiError> {
    let mut params: Vec<ReqParam> = Vec::new();
    println!("{:?}", query.exclude_tag);
    if let Some(tag) = query.include_tag {
        params.extend(filter_multiple_tags(true, tag));
    }
    if let Some(tag) = query.exclude_tag {
        params.extend(filter_multiple_tags(false, tag));
    }
    if let Some(q) = query.query {
        params.push(SEARCH_TITLE(q));
    }
    println!("{:?}", params);

    let res = request_with_params::<ReqParam>(MANGA_BASE_URL.to_string(), params).await?;
    res.to_search_results(false)
}
