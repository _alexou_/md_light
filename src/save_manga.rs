use std::fs;

// use openssl::conf;
use serde::{Deserialize, Serialize};
// use toml::to_string;

// use crate::save_title;

pub fn save_manga(id: String) {
    let mut path = dirs::cache_dir().unwrap();
    path.push("md_light");
    path.push("saved_titles.txt");
    // checking if the id is already present
    let titles = fs::read_to_string(&path).unwrap();
    let mut titles: Vec<&str> = titles.split('\n').collect();
    if !titles.contains(&id.as_str()) {
        titles.push(&id);
        fs::write(path, titles.join("\n")).unwrap();
    }
    println!("{}", id)
}

pub fn check_is_saved(id: String) -> bool {
    let mut path = dirs::cache_dir().unwrap();
    path.push("md_light");
    path.push("saved_titles.txt");

    let titles = fs::read_to_string(path).unwrap();
    let titles: Vec<&str> = titles.split('\n').collect();
    titles.contains(&id.as_str())
}
#[test]
fn tt() {
    unsave_manga("a316c12d-3d43-4ebd-8bde-9f744dd83831".to_string());
}
pub fn unsave_manga(id: String) {
    let mut path = dirs::cache_dir().unwrap();
    path.push("md_light");
    path.push("saved_titles.txt");
    // checking if the id is already present
    let titles = fs::read_to_string(&path).unwrap();
    let mut titles: Vec<&str> = titles.split('\n').collect();

    if titles.contains(&id.as_str()) {
        // let mut i = 0;
        // for title in titles{
        //     if title == id.as_str() {
        //         titles.re
        //     }
        // }
        titles.retain(|&x| x != id.as_str());
    }
    fs::write(path, titles.join("\n")).unwrap();
}

pub fn read_save_file() -> SaveData {
    let mut path = dirs::cache_dir().unwrap();
    path.push("md_light");
    path.push("saved_titles.json");
    // checking if the id is already present
    let titles = match fs::read_to_string(&path) {
        Ok(e) => e,
        Err(_) => "".to_string(),
    };
    match serde_json::from_str(&titles) {
        Ok(e) => e,
        Err(_) => SaveData {
            saved_titles: vec![],
        },
    }
}

pub fn save_to_dict(data: BookmarkedTitles) {
    let mut path = dirs::cache_dir().unwrap();
    path.push("md_light");
    path.push("saved_titles.json");
    // checking if the id is already present
    let titles = match fs::read_to_string(&path) {
        Ok(e) => e,
        Err(_) => "".to_string(),
    };
    let mut dict: SaveData = match serde_json::from_str(&titles) {
        Ok(e) => e,
        Err(_) => SaveData {
            saved_titles: vec![],
        },
    };

    if !dict.saved_titles.contains(&data) {
        dict.saved_titles.push(data);
    }

    fs::write(path, serde_json::to_string_pretty(&dict).unwrap()).unwrap();
}

pub fn check_is_saved_dict(id: String) -> bool {
    let titles = read_save_file();
    for t in titles.saved_titles {
        if t.id == id {
            return true;
        }
    }
    false
}

pub fn unsave_to_dict(id: String) {
    let mut path = dirs::cache_dir().unwrap();
    path.push("md_light");
    path.push("saved_titles.json");
    let mut titles = read_save_file();
    // for t in titles.saved_titles {
    //     if t.id == id {
    //         return true;
    //     }
    // }
    titles.saved_titles.retain(|x| x.id != id.as_str());
    fs::write(path, serde_json::to_string_pretty(&titles).unwrap()).unwrap();
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct SaveData {
    pub saved_titles: Vec<BookmarkedTitles>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct BookmarkedTitles {
    pub id: String,
    pub name: String,
    pub cover_url: String,
}
