use crate::api_error::ApiError;
use crate::language::Language;
use crate::md_struct::*;
use crate::online_md::*;
use crate::req_params::OrderingValues;
use crate::DdlTest;
use serde::Deserialize;
use serde::Serialize;
/// these are the naming conventions that will be used to store the manga and its chapters
/// the manga directory's name will be the manga's name and the website from whit it is downloaded ex: One Piece_MD
/// the chapter folder's names will be ch-1_en, ch-2_en, etc... or similar
/// all of the informations will be stored in a toml file:
/// the toml file will contain :
/// the manga's name, its ID, the website from which it is downloaded (mangaDex, comick, etc...), the description, the tags, the status, the language(s) in which it  is downloaded
/// every downloaded chapter with its name, ID, TL group, chapter number, language, a key to find the corresponding and possibly more
#[derive(Debug, Serialize, Deserialize)]
pub struct OfflineMangaData {
    manga_info: MangaInfo,
    /// the website that is downloaded from eg: mangaDex
    // pub source: Source,
    /// the date at which the manga is first downloaded
    // download_date: DateTime<Local>,
    /// the date of the last download for the manga
    // update_date: DateTime<Local>,
    low_quality_images: bool,
    /// the list of chapters
    chapters: Vec<OfflineChapterData>,
    /// language of downloaded chapters
    lang: Language,
    // the current progress
    // progress_ch: Option<i32>,
    // the total number of chapters at the time of download
    total_chapters: i32,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct OfflineChapterData {
    chapter_id: String,
    chapter_name: Option<String>,
    chapter_number: f64, // #[serde(serialize_with = "serialize_dt")]
                         // date_added: DateTime<Local>,
}

/// downloads the manga
pub async fn fetch_ddl_infos(params: DdlTest) -> Result<OfflineMangaData, ApiError> {
    let lang = params.language;

    let manga_info = get_manga_info(params.manga_id.clone(), true).await?;

    let mut chapters_to_download = vec![];
    let chapter_data = get_manga_chapters(params.manga_id, Some(lang.clone()), 0, OrderingValues::Ascending).await?;

    for ch in chapter_data.chapters {
        let ch = ch?;
        // if &chapter_data.chapters.contains_id(ch.chapter_id){
            
        // }
        let ch_data = OfflineChapterData {
            // chapter_info: ch.clone(),
            chapter_id: ch.chapter_id,
            chapter_name: ch.chapter_name,
            chapter_number: ch.chapter_number, // date_added: chrono::Local::now(),
        };
        chapters_to_download.push(ch_data)
    }

    let offline_manga = OfflineMangaData {
        manga_info,
        // download_date: chrono::Local::now(),
        // update_date: chrono::Local::now(),
        low_quality_images: true,
        chapters: chapters_to_download,
        lang: Language::from("en".to_string()),
        // progress_ch: None,
        total_chapters: chapter_data.total,
    };

    Ok(offline_manga)
}
impl OfflineMangaData {
    pub fn save_download(self) {
        let mut path = dirs::data_local_dir().unwrap();
        path.push("md_light");
        println!("{:#?}", self);

        path.push(self.manga_info.manga_id);
    }
}
