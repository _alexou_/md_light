use std::fmt;
use std::num::ParseIntError;
use std::sync::MutexGuard;
use std::sync::PoisonError;

#[derive(Debug)]
pub enum ApiError {
    Reqwest(reqwest::Error),
    Json(serde_json::Error),
    StrError(String),
    Box(Box<dyn std::any::Any + Send>),
    ParseIntError(ParseIntError),
    FileWriteError(std::io::Error),
    ApiResponseError,
    ApiPageNotFound404,
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            ApiError::Reqwest(_) => write!(
                f,
                "Unable to make a request to the api. Please check your connection"
            ),
            ApiError::Json(err) => write!(f, "json conversion error: {err}"),
            ApiError::StrError(err) => write!(f, "error while processing strings: {err}"),
            ApiError::Box(_) => write!(f, "unknown box error"),
            ApiError::ParseIntError(err) => write!(f, "ParseInt error: {err}"),
            ApiError::FileWriteError(err) => {
                write!(f, "unable to write to file or creating directory: {err}")
            }
            ApiError::ApiResponseError => write!(f, "Got an api response error"),
            ApiError::ApiPageNotFound404 => write!(f, "404 Page not found (Api error)"),
        }
    }
}

// impl std::convert::From<> for ApiError{
//     fn from<T>(err: &str) -> Self {
// todo!()
//     }
// }

impl From<&str> for ApiError {
    fn from(err: &str) -> Self {
        ApiError::StrError(err.to_string())
    }
}
impl From<reqwest::Error> for ApiError {
    fn from(err: reqwest::Error) -> Self {
        ApiError::Reqwest(err)
    }
}

impl From<ParseIntError> for ApiError {
    fn from(err: ParseIntError) -> Self {
        ApiError::ParseIntError(err)
    }
}
impl From<serde_json::Error> for ApiError {
    fn from(err: serde_json::Error) -> Self {
        ApiError::Json(err)
    }
}

impl From<Box<dyn std::error::Error>> for ApiError {
    fn from(_: Box<dyn std::error::Error>) -> Self {
        todo!()
    }
}
impl From<String> for ApiError {
    fn from(err: String) -> Self {
        ApiError::StrError(err)
    }
}
impl From<Box<dyn std::any::Any + Send>> for ApiError {
    fn from(err: Box<dyn std::any::Any + Send>) -> Self {
        ApiError::Box(err)
    }
}
impl From<std::io::Error> for ApiError {
    fn from(err: std::io::Error) -> Self {
        ApiError::FileWriteError(err)
    }
}
impl From<PoisonError<MutexGuard<'_, bool>>> for ApiError {
    fn from(_: PoisonError<MutexGuard<'_, bool>>) -> Self {
        todo!()
    }
}
