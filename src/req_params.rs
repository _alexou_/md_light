use std::clone;

use crate::utills::get_offset_time;
pub type ReqParam = [(&'static str, String); 1];

pub type VecParam<T> = [(&'static str, Vec<T>); 1];

/// includes or excludes a vec ot tags
pub fn filter_multiple_tags(includes_tag: bool, tags: Vec<String>) -> Vec<ReqParam> {
    let mut include_excludes = vec![];
    for tag in tags {
        match includes_tag {
            true => include_excludes.push([("includedTags[]", tag)]),
            false => include_excludes.push([("excludedTags[]", tag)]),
        }
    }
    include_excludes
}

/// excludes chapters with external url (they currently can't load properly)
pub fn exclude_external_url() -> ReqParam {
    [("includeExternalUrl", "0".to_string())]
}

pub fn SEARCH_NAME(name: String) -> ReqParam {
    [("name", name)]
}
pub fn SEARCH_TITLE(name: String) -> ReqParam {
    [("title", name)]
}
pub const HAS_AVAILABLE_CHAPS: fn(bool) -> ReqParam =
    |has_chaps: bool| -> ReqParam { [("hasAvailableChapters", has_chaps.to_string())] };

pub const GET_RECENT: fn() -> ReqParam = || -> ReqParam { [("createdAtSince", get_offset_time())] };
pub const OFFSET: fn(i32) -> ReqParam =
    |offset: i32| -> ReqParam { [("offset", offset.to_string())] };

pub const INCLUDE_TAGS: fn(String) -> ReqParam =
    |tags: String| -> ReqParam { [("includedTags[]", tags)] };

pub const FILTER_LANG: fn(&String) -> ReqParam =
    |lang: &String| -> ReqParam { [("translatedLanguage[]", lang.to_string())] };

pub const FILTER_GROUP: fn(String) -> ReqParam = |group: String| -> ReqParam { [("group", group)] };

pub const FILTER_AUTHOR: fn(String) -> ReqParam =
    |group: String| -> ReqParam { [("authorOrArtist", group)] };

pub const LIMIT: fn(i32) -> ReqParam = |limit: i32| -> ReqParam { [("limit", limit.to_string())] };

pub const INCLUDE_PARAM: fn(IncludedValues) -> ReqParam = |includes: IncludedValues| -> ReqParam {
    let param = match includes {
        IncludedValues::Artist => "artist",
        IncludedValues::Author => "author",
        IncludedValues::ScanlationGroup => "scanlation_group",
        IncludedValues::Cover => "cover_art",
    };
    [("includes[]", param.to_string())]
};

pub const ORDERING: fn(OrderBy, OrderingValues) -> ReqParam =
    |order_by: OrderBy, order: OrderingValues| -> ReqParam {
        let order_by = match order_by {
            OrderBy::Chapter => "order[chapter]",
            OrderBy::FollowedCount => "order[followedCount]",
            OrderBy::Title => "order[title]",
            OrderBy::Year => "order[year]",
            OrderBy::CreatedAt => "order[createdAt]",
            OrderBy::UpdatedAt => "order[updatedAt'",
            OrderBy::Relevance => "order[relevance]",
            OrderBy::LatestUploadedChapter => "order[latestUploadedChapter]",
        };

        let ordering_way = match order {
            OrderingValues::Ascending => "asc",
            OrderingValues::Descending => "desc",
            OrderingValues::Default => "desc",
        };
        // let ord = format!("order[{}]", order_by).as_str();
        [(order_by, ordering_way.to_string())]
    };

pub const FILTER_RATING: fn(ContentRating) -> ReqParam = |rating: ContentRating| -> ReqParam {
    let param = match rating {
        ContentRating::Erotica => "erotica",
        ContentRating::Pornographic => "pornographic",
        ContentRating::Safe => "safe",
        ContentRating::Suggestive => "suggestive",
    };
    [("contentRating[]", param.to_string())]
};

#[derive(Clone, PartialEq)]
/// the order of whatever is requested
pub enum OrderingValues {
    Ascending,
    Descending,
    ///desc
    Default,
}
/// what should the items be ordered by
pub enum OrderBy {
    FollowedCount,
    Chapter,
    Title,
    Year,
    CreatedAt,
    UpdatedAt,
    LatestUploadedChapter,
    Relevance,
}

pub enum IncludedValues {
    Author,
    Artist,
    ScanlationGroup,
    Cover,
}

pub enum ContentRating {
    Safe,
    Suggestive,
    Erotica,
    Pornographic,
}
#[derive(serde::Deserialize)]
pub enum MangaStatus {
    Dropped,
    Completed,
    Ongoing,
}
