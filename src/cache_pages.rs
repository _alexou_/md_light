use actix_web::web::Bytes;
use lazy_static::lazy_static;
use std::{collections::HashMap, sync::RwLock};
lazy_static! {
    static ref CACHED_PAGES: RwLock<HashMap<String, Bytes>> = RwLock::new(HashMap::new());
}

pub fn get_page(id: &String) -> Option<Bytes> {
    match CACHED_PAGES.read() {
        Ok(img) => img.get(id).cloned(),
        Err(_) => None,
    }
}

pub fn insert_page(id: String, data: &Bytes) {
    // println!("inserted url: {:#?}", id);
    if CACHED_PAGES.read().unwrap().len() > 10_000 {
        CACHED_PAGES.write().unwrap().clear();
    }
    CACHED_PAGES.write().unwrap().insert(id, data.clone());
}
