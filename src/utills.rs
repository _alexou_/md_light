use actix_web::HttpRequest;
use chrono::{Datelike, Timelike};
use std::io::{self, Write};

/// returns the local time, offset by a month for the homepage feed
pub fn get_offset_time() -> String {
    let current_time = chrono::Local::now();

    // offsets the time to get the same feed as mangadex's popular titles
    let offset_time = current_time
        .checked_sub_days(chrono::Days::new(30))
        .expect("Time couldn't be correctly offset");

    // 2023-07-08T11 %3A 44 %3A 57
    // 2023-08-07T15 %3A 55 %3A 20
    let formatted_time = format!(
        "{:04}-{:02}-{:02}T{:02}%3A{:02}%3A{:02}",
        offset_time.year(),
        offset_time.month(),
        offset_time.day(),
        offset_time.hour(),
        offset_time.minute(),
        offset_time.second()
    );
    formatted_time
}

/// checks if the request comes from 172.0.0.1 (localhost)
pub fn check_localhost(path: &HttpRequest) -> bool {
    let binding = path.connection_info();
    let ip = binding.peer_addr().expect("unable to get client IP");
    matches!(ip, "172.0.0.1" | "localhost")
}

pub fn prompt_for_bool(message: &str) -> bool {
    let mut input = String::new();
    loop {
        // Print the prompt message
        print!("{} (y/n): ", message);
        io::stdout().flush().expect("Failed to flush stdout");

        // Read the user input
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        let trimmed_input = input.trim().to_lowercase();

        // Check the user input and return the appropriate bool value
        match trimmed_input.as_str() {
            "y" | "yes" => return true,
            "n" | "no" => return false,
            _ => {
                println!("Invalid input. Please enter 'y' or 'n'.");
                input.clear(); // Clear the input buffer for the next iteration
            }
        }
    }
}
// promps for a string
pub fn prompt_for_str(message: &str, validate_input: bool) -> String {
    let mut input = String::new();
    loop {
        // Print the prompt message
        print!("{}", message);
        io::stdout().flush().expect("Failed to flush stdout");

        // Read the user input
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        // makes the user retry if the input is ""
        if validate_input && input.trim() == "" {
            println!("invalid input. Please try again");
            input.clear();
        } else {
            break;
        }
    }
    input.trim().to_string()
}
