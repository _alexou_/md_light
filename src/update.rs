use crate::{online_md, utills};
use reqwest::Error;
use serde::Deserialize;
use std::env::consts::OS;
use std::fs;
use std::process::Command;

#[derive(Deserialize, Debug)]
struct Release {
    tag_name: String,
    assets: Vec<Asset>,
}

#[derive(Deserialize, Debug)]
struct Asset {
    name: String,
    browser_download_url: String,
}

const CURRENT_VERSION: &str = env!("CARGO_PKG_VERSION");

// checks for updates
pub async fn check_for_update() -> Result<(), Error> {
    // fetches for the release versions from github
    let resp: Vec<Release> = online_md::CLIENT
        .get("https://api.github.com/repos/alexou2/md_light/releases")
        .send()
        .await?
        .json()
        .await?;
    let latest_release = &resp[0];

    let curr_i32 = extract_and_convert(CURRENT_VERSION).unwrap();
    let new_i32 = extract_and_convert(&latest_release.tag_name).unwrap();

    println!("Current version: {}", CURRENT_VERSION);
    println!("Latest version: {}", latest_release.tag_name);

    // checks if there is a more recent version
    if latest_release.tag_name != CURRENT_VERSION && curr_i32 < new_i32 {
        println!("New version available: {}", latest_release.tag_name);

        let asset = match OS {
            "linux" => latest_release
                .assets
                .iter()
                .find(|a| a.name.ends_with("Md_Light_release.tar"))
                .unwrap(),
            "windows" => latest_release
                .assets
                .iter()
                .find(|a| a.name.ends_with("Md_Light_release-win.tar"))
                .unwrap(),
            _ => todo!("no release for this version"),
        };
        // confirms before upgrading
        if utills::prompt_for_bool(&format!("Upgrade to V.{}?", latest_release.tag_name)) {
            download_and_replace(&asset.browser_download_url).await?;
        } else {
            println!("update aborted")
        }
    } else {
        println!("You are running the latest version.");
    }

    Ok(())
}

fn extract_and_convert(input: &str) -> Option<i32> {
    let filtered: String = input.chars().filter(|c| c.is_numeric()).collect();
    filtered.parse::<i32>().ok()
}

// downloads and extracts the new release
async fn download_and_replace(url: &str) -> Result<(), Error> {
    let response = reqwest::get(url).await?;
    let bytes = response.bytes().await?;
    println!("{:?}", bytes.len());

    // writes the file to disc
    let path = dirs::home_dir()
        .unwrap()
        .join("MD_Light")
        .join("Md_Light_release.tar");
    fs::write(&path, &bytes).unwrap();

    // Extract the tarball and replace the executable
    Command::new("tar")
        .arg("-xf")
        .arg(path)
        .arg("-C")
        .arg(dirs::home_dir().unwrap())
        .status()
        .expect("failed to extract tarball");

    // // Move the new executable to the correct location (this example assumes the binary is named "myapp")
    // fs::rename("/tmp/myapp", "/usr/local/bin/myapp").unwrap();

    println!("Application updated successfully.");
    Ok(())
}
