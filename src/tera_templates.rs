use crate::api_error::ApiError;
pub use crate::md_struct::*;
use crate::{query_struct, save_manga, tags};
use lazy_static::lazy_static;
use markdown::to_html;
use serde_json::value::{to_value, Value};
use std::collections::HashMap;
use tera::try_get_value;
use tera::{Context, Tera};

// fn custom_function(text: &HashMap<String, Value>) -> Result<String, tera::Error> {
//     // Your custom logic here
//     Ok(format!("Custom function called with input: {:?}", text))
// }

/// filter to convert markdown to html
pub fn markdown(value: &Value, _: &HashMap<String, Value>) -> tera::Result<Value> {
    let mut url = try_get_value!("markdown", "value", String, value);
    url = to_html(&url);

    Ok(to_value(url).unwrap())
}

lazy_static! {
    static ref TEMPLATES: Tera = {

        
        #[cfg(debug_assertions)]
        const PATH: &str = "templates/*";
        // fixes path when building debian package
        #[cfg(not(debug_assertions))]
        const PATH: &str = "/var/lib/md_light/templates/*";


        let mut tera = match Tera::new(PATH) {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                ::std::process::exit(1);
            }
        };
        tera.register_filter("markdown", markdown);

        tera
    };
}

pub fn render_search_templete(
    search_data: SearchResults,
    query: String,
    offset: i32,
    is_localhost: bool,
) -> String {
    let mut context = Context::new();

    context.insert("manga_result", &search_data.search_results);
    context.insert("manga_number", &search_data.total);
    context.insert("query", &query);
    context.insert("current", &round_idx(offset as i64, 20.0));
    context.insert("total", &round_idx(search_data.total, 20.0));
    context.insert("proxy_url", get_proxy_url(is_localhost));

    TEMPLATES
        .render("search_results.html", &context)
        .expect("Failed to render template")
}

pub fn render_author_info(authors: Vec<AuthorInfo>, query: String, offset: i32) -> String {
    let mut context = Context::new();

    context.insert("title", "Search | MD_light");
    context.insert("query", &query);
    context.insert("author_number", &authors.len());
    context.insert("author_list", &authors);
    context.insert("offset", &(offset * 20));

    TEMPLATES
        .render("search.html", &context)
        .expect("Failed to render template")
}

pub fn render_homepage(feed: MdHomepageFeed, is_localhost: bool) -> String {
    let mut context = Context::new();

    context.insert("popular_manga", &feed.currently_popular.search_results);
    context.insert("new_chapters", &feed.new_chapter_releases.search_results);

    context.insert("proxy_url", get_proxy_url(is_localhost));

    TEMPLATES
        .render("new-home.html", &context)
        .expect("Failed to render template")
}

/// renders the manga without the chapters
pub fn render_manga_info(manga: MangaInfo, is_localhost: bool) -> String {
    let mut context = Context::new();

    context.insert("manga_name", &manga.manga_name);

    context.insert("cover", &manga.cover);

    context.insert("description", &manga.description);
    context.insert("authors", &manga.author);
    context.insert("manga_id", &manga.manga_id);
    context.insert("tags", &manga.tags);

    context.insert("proxy_url", get_proxy_url(is_localhost));
    context.insert("is_saved", &save_manga::check_is_saved_dict(manga.manga_id));

    TEMPLATES
        .render("manga_info.html", &context)
        .expect("Failed to render manga info template")
}
/// renders the chapter list of a manga
pub fn render_manga_chapter_list(
    chapters: MangaChapters,
    offset: i32,
    manga_id: String,
    is_localhost: bool,
    render_mode: query_struct::ChapterRenderMode,
) -> Result<String, ApiError> {
    let mut context = Context::new();

    let mut chap: Vec<Chapter> = vec![];
    for ch in chapters.chapters {
        chap.push(ch?)
    }

    context.insert("chapter_list", &chap);
    context.insert("manga_id", &manga_id);
    context.insert("current", &round_idx(offset as i64, 100.0));
    context.insert("total", &round_idx(chapters.total, 100.0));
    context.insert("is_localhost", &is_localhost);

    context.insert("proxy_url", get_proxy_url(is_localhost));

    let rendered_file = match render_mode {
        query_struct::ChapterRenderMode::Normal => "manga_chapter.html",
        query_struct::ChapterRenderMode::Download => "download_chapter.html",
    };

    // let rendered =
    Ok(TEMPLATES.render(rendered_file, &context).unwrap())

    // let ret = match rendered {
    //     Ok(e) => e,
    //     // Err(v) => "An error occured or no chapters are available".to_string(),
    //     Err(v)=> v.to_string(),
    // };
    // Ok(ret)
}

/// renders the page to read the chapters
pub async fn render_chapter_view(
    pages: Vec<String>,
    is_localhost: bool,
    chapter_infos: CurrentChapter,
    manga_id: String,
) -> String {
    let mut context = Context::new();
    // th pages and url
    context.insert("is_localhost", &is_localhost);
    context.insert("chapter", &pages);
    // context.insert("chapter_name", &chapter_infos.curr_chapter_name);
    context.insert("curr_chap", &chapter_infos.curr_chapter);
    context.insert("total", &chapter_infos.total);
    // the contrnt for changing chapters
    context.insert("next_chap", &chapter_infos.next);
    context.insert("has_next", &chapter_infos.next.is_some());
    context.insert("prev_chap", &chapter_infos.prev);
    context.insert("has_prev", &chapter_infos.prev.is_some());

    context.insert("manga_id", &manga_id);

    context.insert("proxy_url", get_proxy_url(is_localhost));

    TEMPLATES
        .render("read_chapter.html", &context)
        .expect("Failed to render chapter template")
}

pub fn render_advanced_search() -> String {
    let mut context = Context::new();

    context.insert("title", "Search | MD_light");
    // let tag = Tag{id:"0234a31e-a729-4e28-9d6a-3f87c4966b9e".to_string(), name:"name".to_string()};
    // let tag2 = Tag{id:"07251805-a27e-4d59-b488-f0bfbec15168".to_string(), name:"name2".to_string()};
    //     let tsgs = [tag, tag2];
    let tags = tags::get_all_tags();
    context.insert("tags_list", &tags);

    TEMPLATES
        .render("advanced_search.html", &context)
        .expect("Failed to render template")
}
pub fn render_autocomplete(
    search_data: SearchResults,
    query: String,
    is_localhost: bool,
) -> String {
    let mut context = Context::new();
    context.insert("title", "Search | MD_light");

    context.insert("manga_result", &search_data.search_results);
    context.insert("query", &query);

    context.insert("proxy_url", get_proxy_url(is_localhost));

    TEMPLATES
        .render("autocomplete.html", &context)
        .expect("Failed to render template")
}

pub fn render_profile_info(data: SearchResults, group_name: String, is_localhost: bool) -> String {
    let mut context = Context::new();
    context.insert("proxy_url", get_proxy_url(is_localhost));
    context.insert("group_name", &group_name);
    context.insert("titles", &data.search_results);

    TEMPLATES
        .render("profile.html", &context)
        .expect("Failed to render chapter template")
}

pub fn render_error_page(error: ApiError, page: &str) -> String {
    let mut context = Context::new();

    context.insert("error", &error.to_string());
    context.insert("requested_page", page);
    // context.insert("proxy_url", get_proxy_url(is_localhost));
    println!("ERROR: {:#?}", error);
    println!("PAGE: {:#?}", page);
    TEMPLATES
        .render("error.html", &context)
        .expect("Failed to render chapter template")
}

pub fn get_server_options() -> String {
    let context = Context::new();

    TEMPLATES
        .render("server.html", &context)
        .expect("Failed to render chapter template")
}
/// shows renders
pub fn render_saved_titles(is_localhost: bool) -> String {
    let mut context = Context::new();
    context.insert("proxy_url", get_proxy_url(is_localhost));
    let titles = save_manga::read_save_file();
    context.insert("saved_titles", &titles.saved_titles);
    TEMPLATES
        .render("saved_titles.html", &context)
        .expect("Failed to render chapter template")
}

/// transforms the offset to an index ex: 501 => 6
fn round_idx(x: i64, round_to: f32) -> i32 {
    let x: f32 = x as f32;
    (x / round_to).ceil() as i32
}
fn get_proxy_url(is_localhost: bool) -> &'static str {
    let mut proxy_url = "";
    if !is_localhost {
        proxy_url = "/proxy/images/"
    }
    proxy_url
}
