// openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'

// use openssl::rsa::Rsa;
// use openssl::x509::{X509, X509NameBuilder};
// use openssl::pkey::PKey;
// use openssl::x509::extension::BasicConstraints;
// use openssl::asn1::Asn1Time;
// use std::fs::File;
// use std::io::Write;
use std::process::Command;

pub fn generate_ssl_cert() {
    // Command::new("mkdir")
    //     .arg("cert")
    //     .status()
    //     .expect("can't create dir");
    Command::new("openssl")
        .arg("req")
        .arg("-x509")
        .arg("-newkey")
        .arg("rsa:4096")
        .arg("-nodes")
        .arg("-keyout")
        .arg("cert/key.pem")
        .arg("-out")
        .arg("cert/cert.pem")
        .arg("-days")
        .arg("365")
        .arg("-subj")
        .arg("/CN=localhost")
        .status()
        .expect("failed to generate keys");

    // openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'
}
