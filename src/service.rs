// ./etc/systemd/system/md_light.service
use std::env;
use std::fs::write;

pub fn generate_service_file() {
    let exec_path = env::current_exe().unwrap();
    let exec_path = exec_path.to_str().unwrap().to_string();
    let curr_path = env::current_dir().unwrap();
    let curr_path = curr_path.to_str().unwrap().to_string();

    let service_str = format!(
        r#"
Description=An mangaDex server with a light frontend
After=network.target
    
[Service]
Type=simple
ExecStart={exec_path} --config
WorkingDirectory={curr_path}

[Install]
WantedBy=default.target"#
    );

    write("/etc/systemd/system/md_light", service_str).expect("unable to write config file");
}
//  Description=An mangaDex server with a light frontend
// After=network.target

// [Service]
// Type=simple
// ExecStart=/home/alexou/md_light -r
// WorkingDirectory=/home/alexou

// [Install]
// WantedBy=default.target
