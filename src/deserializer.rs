use crate::api_error::ApiError;
use crate::language::Language;
use crate::md_struct::*;
use crate::online_md::get_manga_cover;
use crate::req_params;
use crate::req_params::OrderingValues;
use chrono::DateTime;
use chrono::Local;
use serde_json::json;
use serde_json::Value;
use std::str::FromStr;

pub trait MangaDeserializer {
    // fn tt(&self)-> ();
    fn to_search_results(&self, is_low_res: bool) -> Result<SearchResults, ApiError>;
    fn to_manga_info(&self, is_low_res: bool) -> Result<MangaInfo, ApiError>;
    fn to_chapters(&self, ordering: &req_params::OrderingValues)
        -> Result<MangaChapters, ApiError>;
}

impl MangaDeserializer for Value {
    fn to_search_results(&self, is_low_res: bool) -> Result<SearchResults, ApiError> {
        let mut search_results: Vec<ShortMangaInfo> = Vec::new();
        let total = self["total"].as_i64().unwrap();
        let offset = self["offset"].as_i64().unwrap();
        // if the api response is an array, add every manga to the search_results vector
        if let Some(response_data) = self["data"].as_array() {
            for manga in response_data {
                let attributes = &manga["attributes"];

                // getting eery necessary info from the manga
                let manga_id = &manga["id"]
                    .remove_quotes()
                    .ok_or("error while removing quotes")?;

                let title: String = attributes["title"]
                    .as_object()
                    .and_then(|obj| obj.values().next())
                    .ok_or("error while getting title")?
                    .remove_quotes()
                    .ok_or("error while removing quotes")?;
                let status = &attributes["status"]
                    .remove_quotes()
                    .ok_or("error while removing quotes")?;
                let original_language =
                    Language::from(&attributes["originalLanguage"].remove_quotes());
                // let available_languages = attributes["availableTranslatedLanguages"]
                //     .as_array()
                //     .ok_or("error while getting translated languages options")?;
                // let available_languages = Language::to_language_vec(
                //     attributes["availableTranslatedLanguages"].as_array(),
                // );
                let cover = get_manga_cover(manga_id, manga, is_low_res)?;
                let description = &attributes["description"]["en"]
                    .remove_quotes()
                    .unwrap_or("No description".to_string());
                // creating the struct instnce containing all of the usefull ionfos about the manga
                let manga_attributes = ShortMangaInfo {
                    title,
                    id: manga_id.clone(),
                    cover,
                    status: status.clone(),
                    original_language,
                    // translated_languages: available_languages,
                    description: description.clone(),
                };
                search_results.push(manga_attributes)
            }
        }
        Ok(SearchResults {
            search_results,
            total,
            offset,
        })
        // Ok(search_results)
    }

    fn to_manga_info(&self, is_low_res: bool) -> Result<MangaInfo, ApiError> {
        let data = &self["data"];
        let attributes = &data["attributes"];

        let manga_id = data["id"]
            .remove_quotes()
            .ok_or("error while removing quotes")?;

        // gets all of the infos about manga
        let manga_name = attributes["title"]
            .as_object()
            .and_then(|obj| obj.values().next())
            .ok_or("error while getting title")?
            .remove_quotes()
            .ok_or("error while removing quotes in the manga name")?;
        let cover = get_manga_cover(&manga_id, data, is_low_res)?;
        let status = &attributes["status"]
            .remove_quotes()
            .ok_or("error while removing quotes in the status")?;
        let original_language = Language::from(&attributes["originalLanguage"].remove_quotes());
        let description = &attributes["description"]["en"]
            .remove_quotes()
            .unwrap_or("".to_string());
        let year = &attributes["year"].as_i64();

        let mut author_list: Vec<Author> = Vec::new();
        let mut tag_list = Vec::new();
        // let mut translated_language_list = Vec::new();

        // transforming the json part containing the authors into an array
        let author_json = data["relationships"]
            .as_array()
            .ok_or("authors is not an array")?;
        // gets the list pf authors involved in the manga
        for author in author_json {
            // breaks the loop if the data isn't about an author/drawer
            match author["type"].to_string().replace('"', "").as_str() {
                "author" | "artist" => {
                    let author_name = &author["attributes"]["name"]
                        .remove_quotes()
                        .ok_or("error while removing quotes in the author name")?;
                    let author_id = &author["id"]
                        .remove_quotes()
                        .ok_or("error while removing quotes for the author ID")?;
                    let role = &author["type"]
                        .remove_quotes()
                        .ok_or("error while removing quotes in the author role")?;

                    // the author/artist instance
                    let author_instance = Author {
                        author_name: author_name.to_string(),
                        author_id: author_id.to_string(),
                        role: role.to_string(),
                    };
                    author_list.push(author_instance)
                }
                _ => println!("{}", author),
            }
        }

        // getting the tags
        let tag_json = attributes["tags"]
            .as_array()
            .ok_or("tags is not an array")?;
        for tag in tag_json {
            let tag_name = &tag["attributes"]["name"]["en"]
                .remove_quotes()
                .ok_or(format!(
                    "error while removing quotes in the tags: {}",
                    tag["attributes"]["name"]["en"]
                ))?;

            let tag_id = &tag["id"].remove_quotes().ok_or(format!(
                "error while removing quotes in the tags: {}",
                tag["id"]
            ))?;

            let tag = Tag {
                id: tag_id.to_owned(),
                name: tag_name.to_owned(),
            };
            tag_list.push(tag);
        }

        // getting the translation options
        // let translation_options_json = attributes["availableTranslatedLanguages"]
        //     .as_array()
        //     .ok_or("translated_languages is not an array")?;
        // for language in translation_options_json {
        //     let translation = Language::from(language.remove_quotes());
        //     translated_language_list.push(translation);
        // }

        // building the struct with all of the manga's informations+ chapters
        let manga_info = MangaInfo {
            manga_name,
            manga_id,
            author: author_list,
            tags: tag_list,
            cover,
            status: status.clone(),
            original_language,
            // translated_languages: translated_language_list,
            year: *year,
            description: description.clone(),
        };
        Ok(manga_info)
    }

    /// converts json Value to MangaChapters
    fn to_chapters(
        &self,
        ordering: &req_params::OrderingValues,
    ) -> Result<MangaChapters, ApiError> {
        // let mut json_list: Vec<Value> = vec![];
        let list = self["data"]
            .as_array()
            .ok_or("unable to convert chapters to array")?;
        // for i in list {
        //     json_list.push(i.to_owned());
        // }

        // std::fs::write("json.json", self.to_string());

        let total = self["total"].as_i64().unwrap();

        // from_str::<i32>(&self["total"].to_string()).expect("cant't get total chapter number");

        let mut chapter_list: Vec<Result<Chapter, ApiError>> = Vec::new();
        let offset = self["offset"].as_i64().unwrap();
        // let mut idx = total-offset as i64;
        // println!("off {}", offset);
        for (i, chapter) in list.iter().enumerate() {
            let attributes = &chapter["attributes"];
            let chapter_number = &attributes["chapter"];
            // the index of the chapter
            let chap_offset = match ordering {
                OrderingValues::Ascending => i as i64 + offset,
                OrderingValues::Default => total - i as i64 - offset,
                OrderingValues::Descending => total - i as i64 - offset,
            };
            // let chap_offset = i as i64 + offset;

            // println!(" offset {}", chap_offset);

            let chapter_number = chapter_number.remove_quotes(); // if there is no chapter number, set the chapter as a Oneshot

            let chapter_number = match chapter_number {
                Some(e) => {
                    let t: f64 = e.parse().unwrap_or(0.0);
                    // let y = (t *10.0) as i32;
                    // (y/10) as f32
                    t
                }
                None => 0.0,
            };

            let chapter_name = attributes["title"].remove_quotes();
            let language = Language::from(&attributes["translatedLanguage"].remove_quotes());

            let updated_at = attributes["publishAt"].remove_quotes().unwrap();
            let updated_at: DateTime<Local> = chrono::DateTime::from_str(&updated_at).unwrap();
            let chapter_id = chapter["id"]
                .remove_quotes()
                .ok_or("error while removing quotes in the chapter ID")
                .expect("can't get chapterID");

            // getting the translator groups
            let mut tl_group: Vec<TlGroup> = Vec::new();

            let relationships = chapter["relationships"]
                .as_array()
                .ok_or("Unable to convert chapter_relationships into an array");

            if relationships.is_ok() {
                let relationships = relationships.unwrap();
                for relation in relationships {
                    if relation["type"] == json!("scanlation_group") {
                        let group_name = &relation["attributes"]["name"]
                            .remove_quotes()
                            .ok_or("error while removing tl_name quotes");
                        let group_id = relation["id"]
                            .remove_quotes()
                            .ok_or("unable to remove quotes in tl_group id");
                        // cheks if the group's ID or name is an error
                        if group_name.is_ok() && group_id.is_ok() {
                            let name = group_name.as_ref().unwrap();
                            let id = group_id.unwrap();
                            let group = TlGroup {
                                name: name.clone(),
                                id: id.clone(),
                            };
                            tl_group.push(group);
                        }
                    }
                }
            }

            let chapter_instance = Ok(Chapter {
                chapter_name,
                chapter_number,
                language,
                tl_group,
                chapter_id,
                updated_at,
                offset: chap_offset,
                // chapter_idx: 0,
            });
            // idx -= 1;
            chapter_list.push(chapter_instance)
        }

        let ret = MangaChapters {
            chapters: chapter_list,
            total,
        };

        Ok(ret)
    }
}
