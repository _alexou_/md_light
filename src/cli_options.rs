use crate::{installer, utills};
use clap::{Parser, Subcommand};
use colored::Colorize;
use lazy_static::lazy_static;
use local_ip_address::local_ip;
use serde::{Deserialize, Serialize};
use std::io::{self, Write};
use std::net::Ipv4Addr;

lazy_static! {
    /// the startup arguments for the server
    #[derive(Debug)]
    pub static ref CONFIG: CliArgs = get_startup_config();
}

/// loads the config file if the user used --conf
fn get_startup_config() -> CliArgs {
    let mut args = CliArgs::parse();

    //the recommended options are overwritten by the config option
    if args.recommended {
        args.lan = true;
        args.secure = true;
    }

    if args.config {
        println!("Reading the config file...");
        let mut file_path = dirs::config_local_dir().unwrap();
        file_path.push("md_light");
        file_path.push("mdl.conf");

        let config_file = std::fs::read_to_string(file_path);
        match config_file {
            Ok(e) => {
                args = parse_config_file(e);
                println!("reading the file: OK");
            }
            Err(_) => {
                // lets the user change the config file if it is not present
                args = first_setup_process();
                installer::init(&mut args);
                println!("config file created")
            } // println!("Unable to load the config file. Starting with the other arguments"),
        }
    }
    args
}

/// parses the config file
fn parse_config_file(content: String) -> CliArgs {
    println!("{}", content);
    match toml::from_str(&content) {
        Ok(c) => c,
        // lets the user change the default config if the parsng fails
        Err(_) => {
            println!("unable to parse args. the config file may be outdated");
            let mut args = first_setup_process();
            if utills::prompt_for_bool("write to config file?") {
                installer::init(&mut args);
            }
            args
        }
    }
}

/// A web server that uses the mangadex api with a lighweight frontend for potato devices
#[derive(Parser, Serialize, Debug, Deserialize, Clone)]
// #[command(propagate_version = true)]
#[command(author = "_alexou_", version, about, long_about, name = "md_light")]
pub struct CliArgs {
    /// Allows other lan devices to connect to the server (you will need to open the port on your device)
    #[arg(short, long)]
    pub lan: bool,

    /// Uses the lower quality images from mangadex instead of the high quality ones
    #[arg(short, long)]
    pub datasaver: bool,

    /// Uses HttpS instead of Http (more secure)
    #[arg(short, long)]
    pub secure: bool,

    /// Manually set the port for the server
    #[arg(short, long, default_value_t = 8080)]
    pub port: u16,

    /// Uses the recommended server options
    #[arg(short, long)]
    pub recommended: bool,

    /// uses the config file to start the server
    #[arg(short, long)]
    pub config: bool,

    /// bind a specific ip address
    #[arg(short, long)]
    pub ip: Option<String>,

    /// Use verbose output
    #[arg(short, long)]
    pub verbose: bool,

    #[command(subcommand)]
    pub command: Option<Commands>,
}

#[derive(Subcommand, Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum Commands {
    /// Creates the config file for the server. The other parameters used will also be the default params for the config file
    Init,
    /// removes all of the files created by the program
    Uninstall,
    /// Creates the systemd service file for the server.
    Service,
    /// generates the certificates to use https
    Keygen,
    /// checks for updates on github
    Update,
}

impl CliArgs {
    /// returns the server configuration as a CliArgs ref
    pub fn to_args(&self) -> &CliArgs {
        self
    }
}
pub fn first_setup_process() -> CliArgs {
    let lan = utills::prompt_for_bool("use lan?");
    let datasaver = utills::prompt_for_bool("use low quality image?");
    let secure = utills::prompt_for_bool("use https?");
    let mut port: u16 = 8080;
    let mut ip: Option<String> = None;
    if utills::prompt_for_bool("set custom ip address?") {
        ip = Some(prompt_for_ip());
    }
    if utills::prompt_for_bool("set custom port? Default=8080") {
        port = prompt_for_port()
    }
    CliArgs {
        lan,
        datasaver,
        secure,
        port,
        recommended: false,
        config: true,
        ip,
        verbose: false,
        command: None,
    }
}

// fn prompt(message: &str) -> bool {
//     let mut input = String::new();
//     loop {
//         // Print the prompt message
//         print!("{} (y/n): ", message);
//         io::stdout().flush().expect("Failed to flush stdout");

//         // Read the user input
//         io::stdin()
//             .read_line(&mut input)
//             .expect("Failed to read line");
//         let trimmed_input = input.trim().to_lowercase();

//         // Check the user input and return the appropriate bool value
//         match trimmed_input.as_str() {
//             "y" | "yes" => return true,
//             "n" | "no" => return false,
//             _ => {
//                 println!("Invalid input. Please enter 'y' or 'n'.");
//                 input.clear(); // Clear the input buffer for the next iteration
//             }
//         }
//     }
// }

fn prompt_for_port() -> u16 {
    let mut input = String::new();
    loop {
        // Print the prompt message
        print!("Port used: ");
        io::stdout().flush().expect("Failed to flush stdout");

        // Read the user input
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        let trimmed_input = input.trim().to_lowercase();

        // Check the user input and return the appropriate bool value
        match trimmed_input.parse::<u16>() {
            Ok(u) => return u,
            _ => {
                println!("Invalid input. Please enter 'y' or 'n'.");
                input.clear(); // Clear the input buffer for the next iteration
            }
        }
    }
}

fn prompt_for_ip() -> String {
    let mut input = String::new();
    loop {
        // Print the prompt message
        print!("Local ip address. \nType \"local\" to list all available local ip addresses: ");
        io::stdout().flush().expect("Failed to flush stdout");
        // Read the user input
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        let trimmed_input = input.trim().to_lowercase();

        // Check the user input and return the appropriate bool value
        match trimmed_input.parse::<Ipv4Addr>() {
            Ok(_) => return trimmed_input,
            _ => {
                if trimmed_input.as_str() == "local" {
                    println!("{}", local_ip().unwrap().to_string().green());
                } else {
                    println!("Invalid input. Please enter a valid ip address.");
                }

                input.clear(); // Clear the input buffer for the next iteration
            }
        }
    }
}
