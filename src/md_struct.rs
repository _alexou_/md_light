use core::str;

use crate::api_error::ApiError;
use crate::language::Language;
use chrono::{DateTime, Datelike, Local};
use serde::Serializer;
use serde::{Deserialize, Serialize};
use serde_json::Value;

/// used when checking the mangadex api status from /server/ping
pub struct ServerStatus {
    /// is the server down for maintenance
    pub up: bool,
    /// is the server unreachable
    pub reachable: bool,
}
#[derive(Serialize)]
pub struct MdHomepageFeed {
    // pub currently_popular: Vec<PopularManga>,
    pub currently_popular: SearchResults,
    // pub new_chapter_releases: Vec<NewChapters>,
    pub new_chapter_releases: SearchResults,
}

#[derive(Serialize)]
pub struct PopularManga {
    pub title: String,
    pub cover: String,
    pub id: String,
}

#[derive(Serialize)]
pub struct NewChapters {
    pub chapter_name: String,
    // pub chapter_number: String,
    pub chapter_number: f32,
    // pub language: String,
    pub language: Language,
    pub chapter_id: String,
    pub manga_id: String,
    pub tl_group_id: String,
    pub tl_group_name: String,
    pub page_number: String,
}
#[derive(Serialize)]
pub struct SearchResults {
    // pub query: (&'static str, &'static str),
    pub search_results: Vec<ShortMangaInfo>,
    pub total: i64,
    pub offset: i64,
}

#[derive(Serialize)]
pub struct ShortMangaInfo {
    pub title: String,
    pub id: String,
    // pub tags: Vec<Tag>,
    pub cover: String,
    pub status: String,
    pub original_language: Language,
    // pub translated_languages: Vec<Language>,
    pub description: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Tag {
    pub id: String,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug)]
/// the complete informations about ONE specific manga
pub struct MangaInfo {
    pub manga_name: String,
    pub manga_id: String,
    pub author: Vec<Author>,
    pub tags: Vec<Tag>,
    pub cover: String,
    pub status: String,
    pub original_language: Language,
    // pub translated_languages: Vec<Language>,
    pub year: Option<i64>,
    pub description: String,
    // pub chapters: Vec<Result<Chapter, ApiError>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Author {
    pub author_name: String,
    pub author_id: String,
    pub role: String,
}
// #[serde_as]
/// the chapters that are listed in the manga info page
#[derive(Clone, Serialize, Debug)]
pub struct Chapter {
    pub tl_group: Vec<TlGroup>,
    pub chapter_name: Option<String>,
    // pub chapter_number: String,
    pub chapter_number: f64,
    pub offset: i64,
    pub language: Language,
    pub chapter_id: String,
    // pub chapter_idx: i64,
    #[serde(serialize_with = "serialize_dt")]
    pub updated_at: DateTime<Local>,
}

pub fn serialize_dt<S>(date: &DateTime<Local>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let s = time_passed(date.to_owned());

    // let s = format!(r#"{} title="{}""#, s, date);
    serializer.serialize_str(&s)
    //     }
    //     false => serializer.serialize_none(),
    // }
}

fn time_passed(dt: DateTime<Local>) -> String {
    let now: DateTime<Local> = Local::now();
    let duration = now.signed_duration_since(dt);

    // Calculate the number of full years
    let years = now.year() - dt.year();
    // Adjust years if the last year is not completed
    let adjusted_years =
        if now.month() < dt.month() || (now.month() == dt.month() && now.day() < dt.day()) {
            years - 1
        } else {
            years
        };

    // Calculate the number of full months
    let months = (now.year() * 12 + now.month() as i32) - (dt.year() * 12 + dt.month() as i32);
    let adjusted_months = if now.day() < dt.day() {
        months - 1
    } else {
        months
    };

    if adjusted_years > 0 {
        if adjusted_years == 1 {
            format!("{} year ago", adjusted_years)
        } else {
            format!("{} years ago", adjusted_years)
        }
    } else if adjusted_months > 0 {
        if adjusted_months == 1 {
            format!("{} month ago", adjusted_months)
        } else {
            format!("{} months ago", adjusted_months)
        }
    } else if duration.num_weeks() >= 2 {
        let weeks = duration.num_weeks();
        if weeks == 1 {
            format!("{} week ago", weeks)
        } else {
            format!("{} weeks ago", weeks)
        }
    } else if duration.num_days() >= 1 {
        let days = duration.num_days();
        if days == 1 {
            format!("{} day ago", days)
        } else {
            format!("{} days ago", days)
        }
    } else if duration.num_hours() >= 1 {
        let hours = duration.num_hours();
        if hours == 1 {
            format!("{} hour ago", hours)
        } else {
            format!("{} hours ago", hours)
        }
    } else if duration.num_minutes() >= 1 {
        let min = duration.num_minutes();
        if min == 1 {
            format!("{} minute ago", min)
        } else {
            format!("{} minutes ago", min)
        }
    } else if duration.num_seconds() >= 1 {
        let secs = duration.num_seconds();
        if secs == 1 {
            format!("{} second ago", secs)
        } else {
            format!("{} seconds ago", secs)
        }
    } else {
        format!("released at: {}", dt)
    }
}

#[derive(Clone, Serialize, Debug)]
pub struct CurrentChapter {
    //     pub curr_chapter_number: f32,
    //     pub curr_chapter_name: Option<String>,
    pub curr_chapter: Chapter,
    pub prev: Option<Chapter>,
    pub next: Option<Chapter>,
    pub total: i64,
}

#[derive(Serialize)]
pub struct GroupData {
    pub group_id: String,
    pub group_name: String,
    pub titles: Vec<ShortMangaInfo>,
    pub total: i32,
}
#[derive(Debug)]
pub struct MangaChapters {
    pub chapters: Vec<Result<Chapter, ApiError>>,
    pub total: i64,
}

#[derive(Serialize)]
pub struct AuthorInfo {
    pub name: String,
    pub id: String,
    // pub titles_id: Vec<String>,
}

#[derive(Clone, Serialize, Debug, Deserialize)]
pub struct TlGroup {
    pub id: String,
    pub name: String,
}

pub trait ValueExtensions {
    fn remove_quotes(&self) -> Option<String>;
}
impl ValueExtensions for Value {
    fn remove_quotes(&self) -> Option<String> {
        if let Value::String(inner_value) = self {
            Some(inner_value.to_string())
        } else if self.is_number() {
            Some(self.to_string())
        } else {
            None
        }
    }
}
pub trait Rem {
    // fn remove_quotes(tt:Value) -> Option<String> {
    //     if let Value::String(inner_value) = tt {
    //         Some(inner_value.to_string())
    //     } else if tt.is_number() {
    //         Some(tt.to_string())
    //     } else {
    //         None
    //     }
    // }
    fn tt() -> String {
        "ttt".to_string()
    }
}
