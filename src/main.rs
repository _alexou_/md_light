#![allow(warnings)]

mod api_error;
mod cli_options;
mod deserializer;
// mod downloader;
mod installer;
mod language;
// mod manga_templates;
mod cache_pages;
mod endpoints;
mod md_struct;
mod online_md;
mod query_struct;
mod req_params;
mod save_manga;
mod service;
mod ssl_keygen;
mod tags;
mod tera_templates;
mod update;
mod utills;

use actix_files::Files;
use actix_web::{web, App, HttpServer};
use cli_options::*;
use colored::Colorize;
use endpoints::*;
use lazy_static::lazy_static;
use local_ip_address::local_ip;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};
use reqwest::Client;
use std::io::{self, Write};
use std::time::Duration;

lazy_static! {
    static ref CLIENT: Client = Client::new();
}
#[actix_web::main]
async fn main() -> std::io::Result<()> {
tags::fetch_all_tags().await;
    // initializing the server cofiguration
    if let Some(command) = &CONFIG.command {
        match command {
            Commands::Init => installer::init(&mut CONFIG.to_args().clone()),
            Commands::Uninstall => installer::uninstall(),
            Commands::Service => service::generate_service_file(),
            Commands::Keygen => ssl_keygen::generate_ssl_cert(),
            Commands::Update => update::check_for_update().await.unwrap(),
        }
        std::process::exit(0);
    }
    if CONFIG.verbose {
        println!("{:#?}", CONFIG.to_args());
    }

    // creates the server and binding web pages
    let mut server = HttpServer::new(|| {
        App::new()
            .route("/proxy/images/{image_url:.+}", web::get().to(image_proxy))
            .default_service(web::route().to(not_found))
            .service(index)
            .service(search_for_tag)
            .service(kill_server)
            .service(get_server_options)
            .service(get_chapter)
            .service(get_manga_info)
            .service(search)
            .service(autocomplete)
            .service(ping_md)
            .service(advanded_search)
            .service(get_search_results)
            .service(get_author)
            .service(get_chapter_list)
            .service(download_manga)
            .service(get_download_form)
            .service(get_group)
            .service(test_ddl)
            .service(send_error)
            .service(save_title)
            .service(unsave_title)
            .service(save_dict_title)
            .service(get_saved_titles)
            .service(Files::new("/", "/ressources"))
    });

    // the list of all the addresses that will be listened to
    let mut addr_list = vec![];
    // adds the localhost address
    addr_list.push(format!("127.0.0.1:{}", CONFIG.port));

    // binds more addresses if the lan option is used
    if CONFIG.lan {
        // waits until the address is valid before
        while local_ip().is_err() {
            std::thread::sleep(Duration::from_millis(1000))
        }

        let lan_addr = local_ip().unwrap();
        addr_list.push(format!("{}:{}", lan_addr, CONFIG.port));

        // adds a custom ip address
        if let Some(ip) = &CONFIG.ip {
            addr_list.push(format!("{}:{}", ip, CONFIG.port));
        }
    }

    // binds the ip addresses
    for addr in addr_list {
        if CONFIG.secure {
            // reading the certificate and CA
            let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
            builder
                .set_private_key_file("cert/key.pem", SslFiletype::PEM)
                .unwrap();
            builder.set_certificate_chain_file("cert/cert.pem").unwrap();

            // binds the address with ssl certificate
            server = server.bind_openssl(&addr, builder)?;
        } else {
            // binds the address for http only
            server = server.bind(&addr)?;
        }
        println!("Ip: {}", addr);
    }

    println!("{}", "server started successfully".green());
    println!(" {}\n", "with HTTPS".green());
    io::stdout().flush()?;

    server.run().await
}
